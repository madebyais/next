import { randomUUID } from "crypto";
import errors from "errors";
import redis from "libs/redis";
import { IRepository } from "repositories";
import { IAccountRepository } from "repositories/account";
import { IProfileRepository } from "repositories/profile";
import IAccount from "schemas/account";
import IProfile from "schemas/profile";
import Crypt from "utils/crypt";

interface IAccountService {
  register(username: string, password?: string, phoneNo?: string, fullname?: string, dateOfBirth?: any): any;
  login(username: string, password?: string, phoneNo?: string): any;
  resetPassword(username: string): any;
}

export default class AccountService implements IAccountService {
  private accountRepository: IAccountRepository
  private profileRepository: IProfileRepository

  constructor(repository: IRepository) {
    this.accountRepository = repository.db().accountRepository as IAccountRepository
    this.profileRepository = repository.db().profileRepository as IProfileRepository
  }

  public async register(username: string, password?: string, phoneNo?: string, fullname?: string, dateOfBirth?: any) {
    try {
      let [err, data]: any = await this.accountRepository.findByUsernameOrPhoneNo(username, phoneNo)
      if (err && err !== errors.ERR_ACCOUNT_NOT_FOUND) return [err];
      if (data) return [errors.ERR_ACCOUNT_EXIST];

      const crypt = new Crypt();
      const [errEncryptPassword, encryptedPassword]: any = await crypt.setData(password).generateBcrypt(parseInt(process.env.BCRYPT_SALT_ROUNDS!));
      if (errEncryptPassword) return [errEncryptPassword];

      [err, data] = await this.accountRepository.insert(username, encryptedPassword, phoneNo)
      if (err) return [err];

      const account = data as IAccount
      [err, data] = await this.profileRepository.insert(account.id, fullname!, dateOfBirth)
      if (err) return [err];

      return [null, account]
    } catch (e) {
      return [e]
    }
  }

  public async login(username: string, password?: string, phoneNo?: string) {
    if (!password) return [errors.ERR_ACCOUNT_INVALID_PASSWORD];

    try {
      let [err, data]: any = await this.accountRepository.findByUsernameOrPhoneNo(username, phoneNo)
      if (err) return [err];

      const account = data as IAccount
      if (!account.password) return [errors.ERR_ACCOUNT_INVALID_PASSWORD];

      const crypt = new Crypt();
      [err, data] = await crypt.setData(account.password).compareBcrypt(password);
      if (err) return [err];
      if (!data) return [errors.ERR_ACCOUNT_INVALID_PASSWORD];

      [err, data] = await this.profileRepository.findByAccountId(data.id)
      const profile = data as IProfile
      return [null, { id: account.id, username: account.username, fullname: profile.fullname }]
    } catch (e) {
      return [e]
    }
  }

  public async resetPassword(username: string) {
    if (username.length == 0) return [errors.ERR_ACCOUNT_NOT_FOUND]
    try {
      let [err, data]: any = await this.accountRepository.findByUsernameOrPhoneNo(username)
      if (err) return [err];

      const account = data as IAccount

      [err, data] = await this.profileRepository.findByAccountId(account.id)
      if (err) return [err];

      const profile = data as IProfile
      const token = randomUUID()

      await redis.setex(`reset-password:${token}`, 60 * 60, account.username)

      return [null, { username: account.username, fullname: profile.fullname, token }]
    } catch (e) {
      return [errors.ERR_ACCOUNT_GENERATE_RESET_PASSWORD]
    }
  }

  public async patchPassword(token?: string, newPassword?: string) {
    if (!token) return [errors.ERR_ACCOUNT_INVALID_TOKEN_RESET_PASSWORD];
    if (!newPassword) return [errors.ERR_ACCOUNT_INVALID_PASSWORD];

    try {
      let cacheKey: string = `reset-password:${token}`
      let username: any = await redis.get(cacheKey)
      if (!username) return [errors.ERR_ACCOUNT_INVALID_TOKEN_RESET_PASSWORD];

      const crypt = new Crypt();
      const [errEncryptPassword, encryptedPassword]: any = await crypt.setData(newPassword).generateBcrypt(parseInt(process.env.BCRYPT_SALT_ROUNDS!));
      if (errEncryptPassword) return [errEncryptPassword];

      await redis.del(cacheKey)
      await this.accountRepository.updateByUsername(username, { password: encryptedPassword })
      return [null]
    } catch (e) {
      return [errors.ERR_ACCOUNT_PATCH_PASSWORD_FAILED]
    }
  }
}