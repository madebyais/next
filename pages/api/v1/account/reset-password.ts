import Email, { EmailProvider } from "libs/email";
import { NextApiRequest, NextApiResponse } from "next";
import Repository, { IRepository } from "repositories";
import AccountService from "services/account";
import Template from "templates";
import ApiResponse from "utils/api-response";

export default async function v1ResetPassword(req: NextApiRequest, res: NextApiResponse) {
  const repository: IRepository = new Repository()
  const accountService = new AccountService(repository)

  let [err, data]: any = await accountService.resetPassword(req.query.username as string)
  if (err) return new ApiResponse(res, err, {}).json();

  const params = [
    { key: 'fullname', value: data.fullname },
    { key: 'baseUrl', value: process.env.BASE_URL },
    { key: 'token', value: data.token },
  ]

  const template = new Template('email')
  const [errText, text]: any = await template.get('reset-password', 'txt', params)
  if (errText) return new ApiResponse(res, errText, {}).json();
  const [errHtml, html]: any = await template.get('reset-password', 'html', params)
  if (errHtml) return new ApiResponse(res, errHtml, {}).json();

  const email = new Email(process.env.EMAIL_PROVIDER! as EmailProvider)
  const [errSendEmail]: any = await email.send(process.env.EMAIL_FROM!, data.username, 'Reset Password', text, html)

  new ApiResponse(res, errSendEmail, {}).json()
}