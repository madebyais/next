import { NextApiRequest, NextApiResponse } from "next";
import Repository, { IRepository } from "repositories";
import AccountService from "services/account";
import ApiResponse from "utils/api-response";

export default async function v1UpdatePassword(req: NextApiRequest, res: NextApiResponse) {
  const repository: IRepository = new Repository()
  const accountService = new AccountService(repository)

  const { token, newPassword }: any = req.body
  const [err]: any = await accountService.patchPassword(token, newPassword)
  new ApiResponse(res, err, {}).json()
}