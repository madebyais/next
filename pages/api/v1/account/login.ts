import { NextApiRequest, NextApiResponse } from "next";
import Repository, { IRepository } from "repositories";
import AccountService from "services/account";
import ApiResponse from "utils/api-response";
import Crypt from "utils/crypt";
import moment from "moment";
import nookies from "nookies";
import constant from "common/constant";

export default async function v1AccountLogin(req: NextApiRequest, res: NextApiResponse) {
  const repository: IRepository = new Repository()
  const accountService = new AccountService(repository)

  const { username, password, phoneNo }: any = req.body
  const [err, data]: any = await accountService.login(username, password, phoneNo)
  if (err) return new ApiResponse(res, err, data).json();

  const crypt = new Crypt()
  const tokenData: any = { ...data, exp: moment().add(1, 'day').unix() }
  const [errGenerateToken, accessToken]: any = crypt.setData(tokenData).generateJWT(process.env.JWT_SECRET!)

  nookies.set({ res }, constant.cookie.session, accessToken, { maxAge: constant.cookie.maxAge, path: constant.cookie.path })

  new ApiResponse(res, errGenerateToken, { accessToken }).json()
}