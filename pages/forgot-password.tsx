import AccountResetPassword from "components/account/reset-password";

export default function ForgotPassword() {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <AccountResetPassword />
    </div>
  )
}