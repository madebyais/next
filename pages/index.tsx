import I18n, { Ii18n } from "i18n";
import { NextPageContext } from "next";
import { parseCookies } from "nookies";
import PageServerSideProps from "utils/page-server-side-props";

export async function getServerSideProps(ctx: NextPageContext) {
  return PageServerSideProps(ctx)
}

export default function HomePage({ cookies, features }: any) {
  const i18n: Ii18n = new I18n(cookies)
  return (
    <div className="container mx-auto">
      {i18n.get('navigation.menu.login')} <br/>
      {JSON.stringify(features)}
    </div>
  )
}