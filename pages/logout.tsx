import constant from "common/constant";
import { NextPageContext } from "next";
import { destroyCookie } from "nookies";

export async function getServerSideProps(ctx: NextPageContext) {
  destroyCookie(ctx, constant.cookie.session, { path: constant.cookie.path })
  return {
    redirect: {
      destination: "/",
      permanent: false
    }
  }
}

export default function Logout() {
  return <></>
}