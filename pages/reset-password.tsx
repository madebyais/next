import { NextPageContext } from "next";
import AccountUpdatePassword from "components/account/update-password";

export async function getServerSideProps(ctx: NextPageContext) {
  const { token }: any = ctx.query
  return {
    props: {
      token
    }
  }
}

export default function ResetPassword({ token }: any) {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <AccountUpdatePassword token={token} />
    </div>
  )
}