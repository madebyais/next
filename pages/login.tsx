import AccountLogin from "components/account/login";

export default function SignUp() {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <AccountLogin />
    </div>
  )
}