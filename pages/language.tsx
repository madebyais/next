import constant from "common/constant";
import { NextPageContext } from "next";
import nookies from "nookies";

export async function getServerSideProps(ctx: NextPageContext) {
  const { code }: any = ctx.query
  nookies.set(ctx, constant.cookie.language, code, { maxAge: constant.cookie.maxAge, path: constant.cookie.path })  
  return {
    redirect: {
      destination: '/',
      permanent: false
    }
  }
}

export default function Language() {
  return <></>
}