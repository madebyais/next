import AccountSignUp from "components/account/sign-up";

export default function SignUp() {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <AccountSignUp />
    </div>
  )
}