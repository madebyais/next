const getFlag = (key: string) => process.env[`FEATURE_${key}`] && process.env[`FEATURE_${key}`] === 'true' ? true : false

export default {
  login: getFlag("LOGIN")
}