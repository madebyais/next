export default {
  cookie: {
    session: 'next_user_session',
    language: 'next_lang',
    maxAge: 14 * 24 * 60 * 60,
    path: '/'
  }
}