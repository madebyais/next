import errors from "errors";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import log, { Log } from "libs/log";

interface ICrypt {
  setData(data: any): ICrypt;
  generateJWT(secret: string): any;
}

export default class Crypt implements ICrypt {
  private log: Log
  private data: any
  
  constructor() {
    this.log = new Log('utils/crypt')
    this.data = null
  }

  public setData(data: any) {
    this.data = data
    return this
  }

  public generateJWT(secret: string) {
    try {
      return [null, jwt.sign(this.data, secret)]
    } catch (e) {
      this.log.set('generateJWT', JSON.stringify(e)).error()
      return [errors.ERR_CRYPT_GENERATE_JWT]
    }
  }

  public async generateBcrypt(saltRounds: number) {
    try {
      const result: string = await bcrypt.hash(this.data, saltRounds)
      return [null, result]
    } catch (e) {
      this.log.set('generateBcrypt', JSON.stringify(e)).error()
      return [errors.ERR_CRYPT_GENERATE_BCRYPT]
    }
  }

  public async compareBcrypt(plaintext: string) {
    try {
      const result = await bcrypt.compare(plaintext, this.data)
      return [null, result]
    } catch (e) {
      this.log.set('generateBcrypt', JSON.stringify(e)).error()
      return [errors.ERR_CRYPT_COMPARE_BCRYPT]
    }
  }
}