import { Log } from "libs/log";
import { NextApiResponse } from "next";

export default class ApiResponse {
  private res: NextApiResponse
  private error?: any
  private data?: any
  private statusCode: number
  
  constructor (res: NextApiResponse, error?: any, data?: any, statusCode: number = 200) {
    this.res = res
    this.error = error
    this.data = data
    this.statusCode = statusCode
  }

  public json() {
    const log = new Log('utils/api-response')
    if (this.error) {
      log.set('json', this.error).error()
      return this.res.status(this.error.code || 500).json({ success: false, message: this.error.text })
    }
    if (process.env.NODE_ENV !== 'production') log.set('json', this.data).info()
    this.res.status(this.statusCode).json({ success: true, data: this.data })
  }
}