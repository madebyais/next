import feature from "common/feature";
import { NextPageContext } from "next";
import { parseCookies } from "nookies";

export default async function PageServerSideProps (ctx: NextPageContext, fn?: Function) {
  const cookies: any = parseCookies(ctx)
  const features: any = feature
  if (!fn) return { props: { cookies, features } }
  return await fn(ctx)
}