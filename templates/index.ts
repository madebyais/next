import errors from "errors";
import fs from "fs";
import log, { Log } from "libs/log";
import path from "path";

export default class Template {
  private log: Log
  private module: string
  
  constructor(module: string) {
    this.log = new Log('templates/index')
    this.module = module
  }

  public async get(filename: string, filetype: string, params: Array<any>) {
    let data: string = ''
    
    try {
      data = fs.readFileSync(`${process.env.INIT_CWD}/templates/${this.module}/${filename}.${filetype}`).toString()
    } catch (e) {
      this.log.set('get', { error: e, filename, filetype, params }).error()
      return [errors.ERR_TEMPLATE_READ_FAILED]
    }

    params.forEach(item => {
      const re = new RegExp(`{${item.key}}`, 'g')
      data = data.replace(re, item.value)
    })
    return [null, data]
  }
}