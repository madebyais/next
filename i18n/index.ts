import constant from "common/constant";
import { NextPageContext } from "next";
import { parseCookies } from "nookies";
import defaultLang from "./lang/default";
import idLang from "./lang/id";

export interface Ii18n {
  get(key: string): string;
}

export default class I18n implements Ii18n {
  private lang: string
  private i18n: any

  constructor(cookies: any) {
    this.lang = cookies[constant.cookie.language] ? cookies[constant.cookie.language] : 'default'
    this.i18n = {}
    this.setLanguage()
  }

  setLanguage() {
    switch (this.lang) {
      case 'id':
        this.i18n = idLang
        break
      default:
        this.i18n = defaultLang
    }
  }

  public get(key: string): string {
    if (this.i18n[key]) return this.i18n[key]
    return key
  }
}