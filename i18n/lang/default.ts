export default {
  'navigation.menu.login': 'Login',
  'navigation.menu.signup': 'Sign Up',
  'navigation.menu.logout': 'Log Out'
}