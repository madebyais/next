import { Button, Form, Input, Modal } from "antd";
import { useState } from "react";
import Fetch from "utils/fetch";

const AccountUpdatePassword = ({ token }: any) => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isSuccess, setIsSuccess] = useState<boolean>(false)
  
  const onFinish = async (values: any) => {
    setIsLoading(true)

    try {
      values = { ...values, token }
      const resp = await Fetch('POST', `/api/v1/account/update-password`, values)

      if (!resp.success) {
        setIsLoading(false)
        return Modal.error({ title: 'Failed', content: resp.message })
      }

      setIsSuccess(true)
    } catch (e) {
      setIsLoading(false)
      Modal.error({ title: 'Failed', content: 'Something went wrong. Please try again later.' })
    }
  }

  const onFinishFailed = () => {
    Modal.error({ title: 'Error', content: 'Please fill all required fields.' })
  }

  if (isSuccess) {
    return (
      <div style={{width: 400}}>
        Your password has been updated.
      </div>
    )
  }

  return (
    <div style={{width: 400}}>
      <h2 className="mt-6 text-left text-3xl font-extrabold text-gray-900">
        Reset Password
      </h2>
      <div className="text-left mb-5">
        Enter your new password.
      </div>
      <div>
        <Form layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item label="New Password" name="newPassword" required={true} rules={[{ required: true, message: 'New password is required!' }]}>
            <Input.Password />
          </Form.Item>
          {!isLoading && <Button type="primary" htmlType="submit" className="w-full">Submit</Button>}
          {isLoading && <div className="text-center text-gray-500">Updating password ...</div>}
        </Form>
      </div>
    </div>
  )
}

export default AccountUpdatePassword