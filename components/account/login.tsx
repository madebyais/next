import { Button, Form, Input, Modal } from "antd";
import { useState } from "react";
import Fetch from "utils/fetch";

const AccountLogin = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  
  const onFinish = async (values: any) => {
    setIsLoading(true)

    try {
      const resp = await Fetch('POST', '/api/v1/account/login', values)

      if (!resp.success) {
        setIsLoading(false)
        return Modal.error({ title: 'Failed', content: resp.message })
      }

      window.location.href = "/"
    } catch (e) {
      setIsLoading(false)
      Modal.error({ title: 'Failed', content: 'Something went wrong. Please try again later.' })
    }
  }

  const onFinishFailed = () => {
    Modal.error({ title: 'Error', content: 'Please fill all required fields.' })
  }

  return (
    <div style={{width: 400}}>
      <h2 className="mt-6 text-left text-3xl font-extrabold text-gray-900">
        Sign Up
      </h2>
      <div className="text-left mb-5">
        Click <a href="/login" className="font-semibold underline">this link</a> if you already have an account.
      </div>
      <div>
        <Form layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item label="Email Address" name="username" required={true} rules={[{ required: true, message: 'Username is required!' }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Password" name="password" required={true} rules={[{ required: true, message: 'Password is required!' }]}>
            <Input.Password />
          </Form.Item>
          {!isLoading && <Button type="primary" htmlType="submit" className="w-full">Login</Button>}
          {isLoading && <div className="text-center text-gray-500">Please wait ...</div>}
        </Form>
      </div>
    </div>
  )
}

export default AccountLogin