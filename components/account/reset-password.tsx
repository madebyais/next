import { Button, Form, Input, Modal } from "antd";
import { useState } from "react";
import Fetch from "utils/fetch";

const AccountResetPassword = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isSuccess, setIsSuccess] = useState<boolean>(false)
  
  const onFinish = async (values: any) => {
    setIsLoading(true)

    try {
      const { username }: any = values
      const resp = await Fetch('GET', `/api/v1/account/reset-password?username=${username}`)

      if (!resp.success) {
        setIsLoading(false)
        return Modal.error({ title: 'Failed', content: resp.message })
      }

      setIsSuccess(true)
    } catch (e) {
      setIsLoading(false)
      Modal.error({ title: 'Failed', content: 'Something went wrong. Please try again later.' })
    }
  }

  const onFinishFailed = () => {
    Modal.error({ title: 'Error', content: 'Please fill all required fields.' })
  }

  if (isSuccess) {
    return (
      <div style={{width: 400}}>
        A reset password link has been sent to your email.
      </div>
    )
  }

  return (
    <div style={{width: 400}}>
      <h2 className="mt-6 text-left text-3xl font-extrabold text-gray-900">
        Forgot Password
      </h2>
      <div className="text-left mb-5">
        Enter your email address to receive a reset password link.
      </div>
      <div>
        <Form layout="vertical" onFinish={onFinish} onFinishFailed={onFinishFailed}>
          <Form.Item label="Email Address" name="username" required={true} rules={[{ required: true, message: 'Email address is required!' }]}>
            <Input />
          </Form.Item>
          {!isLoading && <Button type="primary" htmlType="submit" className="w-full">Submit</Button>}
          {isLoading && <div className="text-center text-gray-500">Submitting ...</div>}
        </Form>
      </div>
    </div>
  )
}

export default AccountResetPassword