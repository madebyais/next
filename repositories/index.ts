import AccountRepository from "./account"
import ProfileRepository from "./profile"

export interface IRepository {
  db(): any;
}

export default class Repository implements IRepository {
  private _db: any

  constructor() {
    this._db = {
      accountRepository: new AccountRepository(),
      profileRepository: new ProfileRepository()
    }
  }

  public db() {
    return this._db
  }
}