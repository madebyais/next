import { randomUUID } from "crypto";
import errors from "errors";
import prisma from "libs/prisma";

export interface IProfileRepository {
  findById(id: string): any;
  findByAccountId(accountId: string): any;
  insert(accountId: string, fullname: string, dateOfBirth?: any): any;
}

export default class ProfileRepository implements IProfileRepository {
  constructor() {}

  public async findById(id: string) {
    try {
      const result = await prisma.profile.findFirst({ where: { id }})
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }

  public async findByAccountId(accountId: string) {
    try {
      const result = await prisma.profile.findFirst({ where: { accountId }})
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }

  public async insert(accountId: string, fullname: string, dateOfBirth?: any) {
    try {
      const id = randomUUID().toString()
      const result = await prisma.profile.create({ data: { id, accountId, fullname, dateOfBirth } })
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }
}