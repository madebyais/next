import { randomUUID } from "crypto";
import errors from "errors";
import prisma from "libs/prisma";

export interface IAccountRepository {
  findByUsernameOrPhoneNo(username: string, phoneNo?: string): any;
  insert(username: string, password?: string, phoneNo?: string): any;
  updateByUsername(username: string, data: any): any;
}

export default class AccountRepository implements IAccountRepository {
  constructor() { }

  public async findByUsernameOrPhoneNo(username: string, phoneNo?: string) {
    let where: any = {
      username: { equals: username }
    }
    if (phoneNo) {
      where = {
        OR: [
          {username: { equals: username }},
          {phoneNo: { equals: phoneNo }},
        ]
      }
    }

    try {
      const result = await prisma.account.findFirst({ where })
      if (result === null) return [errors.ERR_ACCOUNT_NOT_FOUND]
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }

  public async insert(username: string, password?: string, phoneNo?: string) {
    try {
      const id = randomUUID().toString()
      const result = await prisma.account.create({ data: { id, username, password, phoneNo } })
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }

  public async updateByUsername(username: string, data: any) {
    try {
      const result = await prisma.account.updateMany({ where: { username }, data })
      return [null, result]
    } catch (e) {
      return [errors.ERR_GENERAL_ERROR]
    }
  }
}