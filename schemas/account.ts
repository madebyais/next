import IBase from "./base";
import IProfile from "./profile";

interface IAccount extends IBase {
  username: string;
  password?: string;
  phoneNo?: string;
}

export default IAccount

export interface IAccountProfile extends IAccount, IProfile {}