import IBase from "./base";

interface IProfile extends IBase {
  accountId: string;
  fullname: string;
  dateOfBirth?: any;
}

export default IProfile