interface IBase {
  id: string;
  createdAt: any;
  updatedAt: any;
  deletedAt?: any;
}

export default IBase