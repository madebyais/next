import nodemailer from "nodemailer";
import { IEmail } from "./email";

export default class EmailSmtp implements IEmail {
  private client: nodemailer.Transporter
  
  constructor(opts: any) {
    console.dir(opts)
    this.client = nodemailer.createTransport(opts)
  }

  public async send(from: string, to: string, subject: string, text?: string, html?: string) {
    return await this.client.sendMail({ from, to, subject, text, html })
  }
}