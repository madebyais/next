import errors from "errors";
import { Log } from "./log";
import Mailgun from "./mailgun";
import EmailSmtp from "./smtp";

export interface IEmail {
  send(from: string, to: string, subject: string, text?: string, html?: string): any;
}

export enum EmailProvider {
  SMTP = "smtp",
  MAILGUN = "mailgun"
}

export default class Email implements IEmail {
  private log: Log
  private provider: IEmail
  
  constructor(emailProvider: EmailProvider) {
    this.log = new Log('libs/email')

    switch (emailProvider) {
      case EmailProvider.MAILGUN:
        this.provider = new Mailgun({})
        break
      default:
        this.provider = new EmailSmtp({
          host: process.env.SMTP_HOST,
          port: process.env.SMTP_PORT,
          secure: process.env.SMTP_SECURE === 'true' ? true : false,
          auth: {
            user: process.env.SMTP_USERNAME,
            pass: process.env.SMTP_PASSWORD
          }
        })
    }
  }

  public async send(from: string, to: string, subject: string, text?: string, html?: string) {
    try {
      await this.provider.send(from, to, subject, text, html)
      this.log.set('send', { to, subject, status: 'success' }).info()
      return [null]
    } catch (e) {
      this.log.set('send', { error: JSON.stringify(e), from, to, subject, text, html }).error()
      return [errors.ERR_EMAIL_SENDING_FAILED]
    }
  }
}