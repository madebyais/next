import pino from "pino";

const log = pino()

export default log

export class Log {
  private content: any

  constructor(filename: string) {
    this.content = {
      filename
    }
  }

  public set(method: string, data: any): Log {
    this.content = {
      ...this.content,
      method,
      data
    }

    return this
  }

  public info() {
    log.info({ content: this.content })
  }

  public error() {
    log.error({ content: this.content })
  }
}