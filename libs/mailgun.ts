import FormData from "form-data";
import MailgunJS from "mailgun.js";
import { IEmail } from "./email";

export default class Mailgun implements IEmail {
  private mailgun: MailgunJS
  private opts: any

  constructor(opts: any) {
    this.mailgun = new MailgunJS(FormData)
    this.opts = opts
  }

  public async send(from: string, to: string, subject: string, text?: string, html?: string) {
    const client = this.mailgun.client(this.opts)
    return await client.messages.create(process.env.MAILGUN_DOMAIN!, { from, to, subject, text, html })
  }

}